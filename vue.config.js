module.exports = {
    configureWebpack: {
        resolve: {
            alias: {
                'VariableSass': ('@/assets/styles/variable.scss'),
                'MixinSass': ('@/assets/styles/mixin.scss'),
                'GlobalSass': ('@/assets/styles/global.scss')
            },
            extensions: ['*', '.ts', '.js', '.vue'],
        },
    },
    css: {
        loaderOptions: {
            sass: {

            }
        }
    }
};