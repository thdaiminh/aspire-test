import Vue from 'vue'
import VueRouter from 'vue-router'
import AuthService from "@/services/AuthService";
import userStore from '@/store/users'
Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '',
      component: () => import('@/layouts/CodeChallenge'),
      children: [
        {
          path: '/',
          name: 'Dashboard',
          component: () => import('../views/code-challenge/Dashboard')
        },
        {
          path: '/loan',
          name: 'Add Loans',
          component: () => import('../views/code-challenge/customer/AddLoans')
        },
        {
          path: '/loan-details',
          name: 'Loan Details',
          component: () => import('../views/code-challenge/customer/LoanDetails')
        },
        {
          path: '/loans-list',
          name: 'Loans List',
          component: () => import('../views/code-challenge/admin/LoansList')
        },
        {
          path: '/login',
          name: 'Admin Login',
          component: () => import('../views/code-challenge/Login.vue')
        }
      ]
    },
    {
      path: '',
      component: () => import('@/layouts/CssChallenge'),
      children: [
        {
          path: '/aspire',
          name: 'Cards',
          component: () => import('../views/css-challenge/Cards')
        },
      ],
    }

  ]
})

router.beforeEach((to, from, next) => {
  if (!AuthService.isAuthenticated()) {
    if (to.path === "/login") return next();
    else router.push('/login')
  } else {
    if ((to.path === '/loan' || to.path === '/loan-details') && userStore.state.user.role === 1){
      router.push('/');
    }
    if (to.path === '/loans-list' && userStore.state.user.role !== 1){
      router.push('/');
    }
    if (to.path === "/login") router.push('/')
    else return next();
  }
});

export default router
