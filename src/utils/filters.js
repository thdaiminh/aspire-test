export const formatPrice = (value) => {
    if (!value) {
        return 0;
    }
    return value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
}

export const yearSuffix = (value) => {
    if (!value) {
        return null;
    }
    if (value === 1) return value + ' year'
    else return value + ' years'
}

export const formatDate = (value) => {
    if (!value) {
        return null;
    }
    value = new Date(value);
    return ('0' + value.getDate()).slice(-2) + '/'
        + ('0' + (value.getMonth()+1)).slice(-2) + '/'
        + value.getFullYear();
}

export const formatDueDate = (value) => {
    if (!value) {
        return null;
    }
    value = new Date(value);
    return ('0' + value.getDate()+7).slice(-2) + '/'
        + ('0' + (value.getMonth()+1)).slice(-2) + '/'
        + value.getFullYear();
}