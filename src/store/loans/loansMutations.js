export const loansMutationDef = Object.freeze({
    SET_LOADING: 'setLoading',
    SET_LIST_OPTIONS: 'setListOptions',
    SET_LIST_LOANS: 'setListLoans',
});

export default {
    [loansMutationDef.SET_LOADING](state, payload) {
        state.loading = payload;
    },

    [loansMutationDef.SET_LIST_OPTIONS](state, payload) {
        state.listOptions = payload;
    },

    [loansMutationDef.SET_LIST_LOANS](state, payload) {
        state.listLoans = payload;
    }
}
