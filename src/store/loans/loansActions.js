import axios from "axios";
import { loansMutationDef } from "@/store/loans/loansMutations";
import { INTEREST_RATE } from "@/utils/constance";

export const loansActionDef = Object.freeze({
    GET_LIST_OPTIONS: 'getListOptions',
    GET_LIST_LOANS: 'getListLoans',
    STORE_LOANS: 'storeLoans',
    CHANGE_STATUS: 'changeStatus',
    PAY: 'payLoan'
});

const API_URL = ' http://localhost:3000';

export default {
    [loansActionDef.GET_LIST_OPTIONS]({commit}) {
        return new Promise((resolve, reject) => {
            commit(loansMutationDef.SET_LOADING, true);
            axios.get(`${API_URL}/loansFields`)
                .then(response => {
                commit(loansMutationDef.SET_LOADING, false);
                commit(loansMutationDef.SET_LIST_OPTIONS, response.data);
                resolve(response.data);
            }).catch(err => {
                commit(loansMutationDef.SET_LOADING, false);
                reject(err);
            });
        });
    },
    [loansActionDef.GET_LIST_LOANS]({commit}, params = null) {
        return new Promise((resolve, reject) => {
            commit(loansMutationDef.SET_LOADING, true);
            axios.get(`${API_URL}/loansList`, {
                params: params,
            })
                .then(response => {
                    commit(loansMutationDef.SET_LOADING, false);
                    commit(loansMutationDef.SET_LIST_LOANS, response.data);
                    resolve(response.data);
                }).catch(err => {
                commit(loansMutationDef.SET_LOADING, false);
                reject(err);
            });
        });
    },
    [loansActionDef.STORE_LOANS]({commit, dispatch}, params) {
        return new Promise((resolve, reject) => {
            commit(loansMutationDef.SET_LOADING, true);
            axios.post(`${API_URL}/loansList`, params)
                .then(response => {
                    dispatch(loansActionDef.GET_LIST_LOANS);
                    commit(loansMutationDef.SET_LOADING, false);
                    resolve(response.data);
                }).catch(err => {
                commit(loansMutationDef.SET_LOADING, false);
                reject(err);
            });
        });
    },
    [loansActionDef.CHANGE_STATUS]({commit, dispatch}, params) {
        return new Promise((resolve, reject) => {
            commit(loansMutationDef.SET_LOADING, true);
            const loanId = params.id;
            axios.put(`${API_URL}/loansList/${loanId}`, {...params, status: 2})
                .then(response => {
                    dispatch(loansActionDef.GET_LIST_LOANS);
                    commit(loansMutationDef.SET_LOADING, false);
                    resolve(response.data);
                }).catch(err => {
                commit(loansMutationDef.SET_LOADING, false);
                reject(err);
            });
        });
    },
    [loansActionDef.PAY]({commit, dispatch}, params) {
        return new Promise((resolve, reject) => {
            commit(loansMutationDef.SET_LOADING, true);
            const loanId = params.id;
            let totalPayment = params.amount + ((params.amount * INTEREST_RATE) / 100);
            if (params.paid >= totalPayment) {
                params = {...params, status: 3}
            }
            axios.put(`${API_URL}/loansList/${loanId}`, params)
                .then(response => {
                    dispatch(loansActionDef.GET_LIST_LOANS);
                    commit(loansMutationDef.SET_LOADING, false);
                    resolve(response.data);
                }).catch(err => {
                commit(loansMutationDef.SET_LOADING, false);
                reject(err);
            });
        });
    },
}