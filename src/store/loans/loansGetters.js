export default {
    getListOptions(state) {
        return state.listOptions;
    },

    getListLoans(state) {
        return state.listLoans;
    },

    getLoading(state) {
        return state.loading;
    }
}
