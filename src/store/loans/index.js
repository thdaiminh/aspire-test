import loansActions from "@/store/loans/loansActions";
import loansMutations from "@/store/loans/loansMutations";
import loansGetters from "@/store/loans/loansGetters";
import loansState from "@/store/loans/loansState";

export default {
    namespaced: true,
    actions: loansActions,
    mutations: loansMutations,
    getters: loansGetters,
    state: loansState,
}
