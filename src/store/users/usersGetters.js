export default {
    isLoggedIn() {
        return false;
    },
    getLoading(state) {
        return state.loading;
    },
    getUser(state) {
        return state.user;
    },
    getListUsers(state){
        return state.users;
    }
}
