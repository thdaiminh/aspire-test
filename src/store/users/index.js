import usersActions from "@/store/users/usersActions";
import usersMutations from "@/store/users/usersMutations";
import usersGetters from "@/store/users/usersGetters";
import usersState from "@/store/users/usersState";

export default {
    namespaced: true,
    actions: usersActions,
    mutations: usersMutations,
    getters: usersGetters,
    state: usersState,
}
