import AuthService from "@/services/AuthService";

export const usersMutationDef = Object.freeze({
    SET_TOKEN: 'setToken',
    UNSET_TOKEN: 'removeToken',
    SET_LOADING: 'setLoading',
    SET_PROFILE: 'setCurrentProfile',
    SET_LIST: 'setListUsers'
})

export default {
    [usersMutationDef.SET_TOKEN](state, payload) {
        AuthService.storeToken(payload.token);
    },

    [usersMutationDef.UNSET_TOKEN](state) {
        AuthService.removeToken();
        state.user = {};
    },

    [usersMutationDef.SET_LOADING](state, payload) {
        state.loading = payload;
    },

    [usersMutationDef.SET_PROFILE](state, payload) {
        state.user = payload;
    },

    [usersMutationDef.SET_LIST](state, payload) {
        state.users = payload;
    },
}
