import axios from "axios";
import { usersMutationDef } from "@/store/users/usersMutations";
import router from "../../router";
import { TOKEN_NAME } from "@/services/AuthService";

export const usersActionsDef = Object.freeze({
    AUTHENTICATE: 'authenticate',
    LOGOUT: 'logout',
    FETCH: 'getCurrentUser',
    GET_LIST: 'getListUsers',
    STORE: 'store',
    ADD_BALANCE: 'addBalance',
    MINUS_BALANCE: 'minusBalance'
})

const API_URL = 'http://localhost:3000/users';

export default {
    [usersActionsDef.AUTHENTICATE]({commit}, payload) {
        commit(usersMutationDef.SET_LOADING, true);
        return new Promise((resolve, reject) => {
            axios.get(API_URL)
                .then(response => {
                    let matchUser = response.data.filter(user => user.email === payload.email && user.password === payload.password);
                    if(matchUser.length === 1){
                        commit(usersMutationDef.SET_TOKEN, {
                            token: matchUser[0].id
                        });
                        commit(usersMutationDef.SET_PROFILE, matchUser);
                        commit(usersMutationDef.SET_LOADING, false);
                        resolve(true);
                    }
                    else {
                        commit(usersMutationDef.SET_LOADING, false);
                        resolve(false);
                    }
                })
                .catch(error => {
                    commit(usersMutationDef.SET_LOADING, false);
                    reject(error);
                })
        });
    },

    [usersActionsDef.LOGOUT]({commit}) {
        commit(usersMutationDef.UNSET_TOKEN);
        router.push('/login');
    },

    [usersActionsDef.FETCH]({commit}) {
        commit(usersMutationDef.SET_LOADING, true);
        return new Promise((resolve, reject) => {
            const userId = localStorage.getItem(TOKEN_NAME);
            axios.get(`${API_URL}/${userId}`)
                .then(response => {
                    commit(usersMutationDef.SET_PROFILE, response.data);
                    commit(usersMutationDef.SET_LOADING, false);
                    resolve(response.data);
                })
                .catch(error => {
                    commit(usersMutationDef.SET_LOADING, false);
                    reject(error);
                })
        });
    },

    [usersActionsDef.GET_LIST]({commit}) {
        commit(usersMutationDef.SET_LOADING, true);
        return new Promise((resolve, reject) => {
            axios.get(API_URL)
                .then(response => {
                    commit(usersMutationDef.SET_LIST, response.data);
                    commit(usersMutationDef.SET_LOADING, false);
                    resolve(response.data);
                })
                .catch(error => {
                    commit(usersMutationDef.SET_LOADING, false);
                    reject(error);
                })
        });
    },

    [usersActionsDef.STORE]({commit, dispatch}, payload) {
        return new Promise((resolve, reject) => {
            commit(usersMutationDef.SET_LOADING, true);
            axios.post(API_URL, {...payload, role: 2})
                .then(response => {
                    commit(usersMutationDef.SET_LOADING, false);
                    commit(usersMutationDef.SET_TOKEN, {
                        token: response.data.id
                    });
                    dispatch(usersActionsDef.FETCH);
                    resolve(true);
                })
                .catch(error => {
                    commit(usersMutationDef.SET_LOADING, false);
                    reject(error.response);
                })
        })
    },

    [usersActionsDef.ADD_BALANCE]({commit, dispatch, state}, {amount, userId}) {
        return new Promise((resolve, reject) => {
            commit(usersMutationDef.SET_LOADING, true);
            dispatch(usersActionsDef.GET_LIST);
            let matchUser = state.users.filter(user => user.id === userId);
            axios.put(`${API_URL}/${matchUser[0].id}`, {...matchUser[0], balance: matchUser[0].balance + amount })
                .then(response => {
                    commit(usersMutationDef.SET_LOADING, false);
                    dispatch(usersActionsDef.FETCH);
                    resolve(response);
                })
                .catch(error => {
                    commit(usersMutationDef.SET_LOADING, false);
                    reject(error.response);
                })
        })
    },

    [usersActionsDef.MINUS_BALANCE]({commit, dispatch, state}, params) {
        return new Promise((resolve, reject) => {
            commit(usersMutationDef.SET_LOADING, true);
            const currentUser = state.user;
            axios.put(`${API_URL}/${currentUser.id}`, {...currentUser, balance: currentUser.balance - params })
                .then(response => {
                    commit(usersMutationDef.SET_LOADING, false);
                    dispatch(usersActionsDef.FETCH);
                    resolve(response);
                })
                .catch(error => {
                    commit(usersMutationDef.SET_LOADING, false);
                    reject(error.response);
                })
        })
    },
}
