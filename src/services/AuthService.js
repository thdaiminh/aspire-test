import EventEmitter from 'events'

export const TOKEN_NAME = 'access_token';
const LOGIN_EVENT = 'loggedIn';

class AuthService extends EventEmitter {
    logOut() {
        localStorage.removeItem(TOKEN_NAME);

        this.emit(LOGIN_EVENT, {loggedIn: false});
    }

    isAuthenticated() {
        const accessToken = localStorage.getItem(TOKEN_NAME);
        if (accessToken) {
            return true;
        }
        return false;
    }

    storeToken(token) {
        localStorage.setItem(TOKEN_NAME, token);
    }

    removeToken() {
        localStorage.removeItem(TOKEN_NAME);
    }

    getToken() {
        return localStorage.getItem(TOKEN_NAME);
    }
}

export default new AuthService();
