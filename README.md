# aspire-test

## Project setup
```
npm install
```

## Compiles and hot-reloads for landing page 
```
npm run serve
```

## Run json server for mock data
```
cd src\mock-data
json-server --watch mock.json
```

## Route
Front-end
- Code Challenge: `http://localhost:8080`
- Css Challenge: `http://localhost:8080/aspire`

Mock Server
- Json Server: `http://localhost:3000`


## Account list
Client
- Email: `client@mail.com`
- Password: `client`

Admin (For approval)
- Email: `admin@mail.com`
- Password: `admin`

